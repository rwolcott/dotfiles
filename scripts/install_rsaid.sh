#!/bin/bash

SSH_DIR=$HOME/.ssh      # ssh directory
START_DIR=`pwd`

echo "Installing RSA ID"
mkdir -p $SSH_DIR
cd $SSH_DIR
gpg id_rsa.gpg
chmod 600 id_rsa

cd $START_DIR
