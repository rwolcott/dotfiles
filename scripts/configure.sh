#!/bin/bash
############################
# .make.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
############################


##########

read -p "Install dotfiles (y/n)? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    bash install_dotfiles.sh
fi

# let's install vim plugins
read -p "Install vim plugins (y/n)? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    bash install_vim.sh
fi

# and now ctags
read -p "Install ctags (y/n)? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    bash install_ctags.sh
fi

# and now decrypt key
read -p "Install RSA ID (y/n)? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    bash install_rsaid.sh
fi

