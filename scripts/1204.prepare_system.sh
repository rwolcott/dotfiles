#!/bin/bash
PACKAGES=""
function addpkg {
    PACKAGES="$PACKAGES $@"
}
function ask_yn() { # {prompt question}
    read -p "$1" -n 1 -r
    if [[ $REPLY != "" ]]; then
        echo
    fi
}
function set_gconf_string() { # {key, value}
    echo "gconf_string: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type string "$2" &>> $INSTALL_LOG
}
function set_gconf_bool() { # {key, value}
    echo "gconf_bool: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type bool $2 &>> $INSTALL_LOG
}
function set_gconf_float() { # {key, value}
    echo "gconf_float: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type float $2 &>> $INSTALL_LOG
}
function set_gconf_int() { # {key, value}
    echo "gconf_int: <$1> <$2>"
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type int $2
}

if [ `id -u` -ne "0" ]; then
	echo "You must be root to run this script"
	exit 1
fi;



#================================================
# Batch prompt what to install..
#================================================


ask_yn "Would you like to configure shortcuts? [Y/n] "
prompt_install_shortcuts=$REPLY
ask_yn "Would you like to install third-party apps? [Y/n] "
prompt_install_thirdparty=$REPLY
ask_yn "Would you like to install dotfiles? [Y/n] "
prompt_install_dotfiles=$REPLY
ask_yn "Would you like to install rsaid? [Y/n] "
prompt_install_rsaid=$REPLY
ask_yn "Would you like to install vimplugins? [Y/n] "
prompt_install_vimplugins=$REPLY
ask_yn "Would you like to install software repos.? [Y/n] "
prompt_install_softwarerepos=$REPLY
ask_yn "Would you like to install personal repos.? [Y/n] "
prompt_install_personalrepos=$REPLY
ask_yn "Would you like to configure compiz? [Y/n] "
prompt_install_compiz=$REPLY


echo ""
echo "Installation will start in 5 seconds.."
sleep 5

INIT_DIR=`pwd`
INSTALL_LOG=/var/tmp/setup.log
export HOME=/home/$SUDO_USER
source $HOME/.bashrc &>> $INSTALL_LOG
cd $HOME

echo ""
echo ""
echo "You may monitor a more detailed output of the setup with"
echo "       tail -f $INSTALL_LOG"
echo ""
echo ""

#=========================================================================
# 0) Obtain dbus address hack for setting gconf under sudo
#=========================================================================
DBUS_SESSION_BUS_ADDRESS=$(sudo -u $SUDO_USER dbus-launch --autolaunch=`cat /var/lib/dbus/machine-id` | grep BUS_ADDRESS | cut -d '=' -f 2-)

#=========================================================================
# 1) Setup shortcuts and configs
#=========================================================================
if [[ $prompt_install_shortcuts =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Setting up shortcuts and configs.."
    echo "Setting up shortcuts and configs.." > $INSTALL_LOG

    #-fonts
    set_gconf_string /desktop/gnome/interface/document_font_name "Sans 10"
    set_gconf_string /desktop/gnome/interface/monospace_font_name "Ubuntu Mono 11"
    set_gconf_string /desktop/gnome/interface/font_name "Ubuntu 10"
    set_gconf_string /apps/metacity/general/titlebar_font "Ubuntu Bold 10"
    set_gconf_string /apps/nautilus/preferences/desktop_font "Ubuntu 11"

    #-terminal settings
    set_gconf_bool /apps/gnome-terminal/profiles/Default/scrollback_unlimited 1
    set_gconf_bool /apps/gnome-terminal/profiles/Default/use_system_font 0
    set_gconf_string /apps/gnome-terminal/profiles/Default/font "Ubuntu Mono derivative Powerline 9"
    set_gconf_float /apps/gnome-terminal/profiles/Default/background_darkness 1
    set_gconf_string /apps/gnome-terminal/profiles/Default/background_type "solid"
    set_gconf_bool /apps/gnome-terminal/profiles/Default/use_theme_colors 0
    set_gconf_string /apps/gnome-terminal/profiles/Default/palette "#2E2E34343636:#CCCC00000000:#4E4E9A9A0606:#C4C4A0A00000:#34346565A4A4:#757550507B7B:#060698209A9A:#D3D3D7D7CFCF:#555557575353:#EFEF29292929:#8A8AE2E23434:#FCFCE9E94F4F:#72729F9FCFCF:#ADAD7F7FA8A8:#3434E2E2E2E2:#EEEEEEEEECEC"
    set_gconf_string /apps/gnome-terminal/profiles/Default/background_color "#000000000000"
    set_gconf_string /apps/gnome-terminal/profiles/Default/foreground_color "#FFFFFFFFFFFF"

    #-default browser
    set_gconf_string /desktop/gnome/applications/browser/exec "/opt/google/chrome/google-chrome"

    #-workspace movement (ctrl+shift+[HJKL])
    set_gconf_string /apps/metacity/global_keybindings/switch_to_workspace_up "<Primary><Shift>k"
    set_gconf_string /apps/metacity/global_keybindings/switch_to_workspace_right "<Primary><Shift>l"
    set_gconf_string /apps/metacity/global_keybindings/switch_to_workspace_left "<Primary><Shift>h"
    set_gconf_string /apps/metacity/global_keybindings/switch_to_workspace_down "<Primary><Shift>j"

    #-workspace window movement (ctrl+shift+alt+[HJKL])
    set_gconf_string /apps/metacity/window_keybindings/move_to_workspace_up "<Primary><Shift><Alt>k"
    set_gconf_string /apps/metacity/window_keybindings/move_to_workspace_right "<Primary><Shift><Alt>l"
    set_gconf_string /apps/metacity/window_keybindings/move_to_workspace_left "<Primary><Shift><Alt>h"
    set_gconf_string /apps/metacity/window_keybindings/move_to_workspace_down "<Primary><Shift><Alt>j"

    #-new terminal (ctrl+shift+X)
    set_gconf_string /apps/metacity/global_keybindings/run_command_terminal "<Primary><Shift>x"

    #-toggle maximization (ctrl+shift+Z)
    set_gconf_string /apps/metacity/window_keybindings/toggle_maximized "<Primary><Shift>z"

    #-media key redirects using xdotool
    set_gconf_string /desktop/gnome/keybindings/custom0/name "Volume Down Ryan"
    set_gconf_string /desktop/gnome/keybindings/custom0/binding "<Primary><Shift>u"
    set_gconf_string /desktop/gnome/keybindings/custom0/action "xdotool key --clearmodifiers --delay 0 XF86AudioLowerVolume"
    set_gconf_string /desktop/gnome/keybindings/custom1/name "Volume Up Ryan"
    set_gconf_string /desktop/gnome/keybindings/custom1/binding "<Primary><Shift>i"
    set_gconf_string /desktop/gnome/keybindings/custom1/action "xdotool key --clearmodifiers --delay 0 XF86AudioRaiseVolume"
    set_gconf_string /desktop/gnome/keybindings/custom2/name "Next Track Ryan"
    set_gconf_string /desktop/gnome/keybindings/custom2/binding "<Primary><Shift>o"
    set_gconf_string /desktop/gnome/keybindings/custom2/action "xdotool key --clearmodifiers --delay 0 XF86AudioNext"
    set_gconf_string /desktop/gnome/keybindings/custom3/name "Previous Track Ryan"
    set_gconf_string /desktop/gnome/keybindings/custom3/binding "<Primary><Shift>y"
    set_gconf_string /desktop/gnome/keybindings/custom3/action "xdotool key --clearmodifiers --delay 0 XF86AudioPrev"
    set_gconf_string /desktop/gnome/keybindings/custom4/name "Play/Pause Ryan"
    set_gconf_string /desktop/gnome/keybindings/custom4/binding "<Primary><Shift>p"
    set_gconf_string /desktop/gnome/keybindings/custom4/action "xdotool key --clearmodifiers --delay 0 XF86AudioPlay"

    #-remove user name from panel
    gsettings set com.canonical.indicator.session show-real-name-on-panel false &>> $INSTALL_LOG

fi





#=========================================================================
# 2) Install 3rd party apps
#=========================================================================

if [[ $prompt_install_thirdparty =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Updating System"
    echo "Updating System" > $INSTALL_LOG

    # Add extra repositories
    add-apt-repository -y ppa:pithos/ppa &>> $INSTALL_LOG
    add-apt-repository -y ppa:synapse-core/testing &>> $INSTALL_LOG

    # First, lets get up-to-date
    if !(apt-get -yq update &>> $INSTALL_LOG); then
        echo "apt-get update failure"
        exit 1
    fi;

    apt-get install -yq python-software-properties &>> $INSTALL_LOG

    # Personal packages
    addpkg \
        pithos \
        synapse \
        vlc \
        vim-gnome \
        subversion \
        ssh \
        openssh-server \
        git-core \
        gitk \
        gitg \
        xchat \
        ant \
        gtk-doc-tools \
        libglib2.0-dev \
        libusb-1.0-0-dev \
        gv \
        libncurses-dev \
        libncurses5-dev \
        autopoint \
        libgl1-mesa-dev \
        openjdk-6-jdk \
        socat \
        ctags \
        inkscape \
        virtualbox \
        openssh-client \
        openssh-server \
        pkg-config \
        scons \
        sysstat \
        make \
        ruby \
        python \
        python-indicate \
        python-notify \
        apt-file \
        colormake \
        indicator-multiload \
        indicator-cpufreq \
        valgrind \
        kcachegrind \
        meld \
        xdotool \
        minidlna \
        gconf-editor \
        screen \
        pv


    echo "Installing essential packages"
    echo "Installing essential packages" &>> $INSTALL_LOG
    # General installations
    echo "apt-get install $PACKAGES"
    echo "apt-get install $PACKAGES" &>> $INSTALL_LOG
    apt-get update &>> $INSTALL_LOG
    apt-get -yq install $PACKAGES &>> $INSTALL_LOG || \
        { \
            echo "Program Installation Failed"; \
            exit 1; \
        }

    # additional apps that require downloaded deb
    # GOOGLE CHROME
    if ! which google-chrome > /dev/null 2>&1; then
        echo "Installing google-chrome"
        echo "Installing google-chrome" >> $INSTALL_LOG
        wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb &>> $INSTALL_LOG
        if [ -e "google-chrome-stable_current_amd64.deb" ]; then
            echo "google-chrome download finished"
            echo "google-chrome download finished" &>> $INSTALL_LOG
            dpkg -i google-chrome-stable_current_amd64.deb &>> $INSTALL_LOG
            rm google-chrome-stable_current_amd64.deb &>> $INSTALL_LOG
        else
            echo "google-chrome didn't download"
            echo "google-chrome didn't download" &>> $INSTALL_LOG
            exit 1;
        fi
    else
        echo "google-chrome installed"
        echo "google-chrome installed" &>> $INSTALL_LOG
    fi

    # MENDELEY
    if ! which mendeleydesktop > /dev/null 2>&1; then
        echo "Installing mendeley"
        echo "Installing mendeley" >> $INSTALL_LOG
        wget http://www.mendeley.com/repositories/ubuntu/stable/amd64/mendeleydesktop-latest &>> $INSTALL_LOG
        if [ -e "mendeleydesktop-latest" ]; then
            echo "mendeleydesktop download finished"
            echo "mendeleydesktop download finished" &>> $INSTALL_LOG
            dpkg -i mendeleydesktop-latest &>> $INSTALL_LOG
            rm mendeleydesktop-latest &>> $INSTALL_LOG
        else
            echo "mendeleydesktop didn't download"
            echo "mendeleydesktop didn't download" &>> $INSTALL_LOG
            exit 1;
        fi
    else
        echo "mendeleydesktop installed"
        echo "mendeleydesktop installed" &>> $INSTALL_LOG
    fi

    # SPOTIFY
    if ! which spotify > /dev/null 2>&1; then
        echo "Installing spotify"
        echo "Installing spotify" >> $INSTALL_LOG
        echo "deb http://repository.spotify.com stable non-free" >> /tmp/spotify-client.list
        cp /tmp/spotify-client.list /etc/apt/sources.list.d/
        rm /tmp/spotify-client.list
        apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 94558F59 &>> $INSTALL_LOG
        apt-get update &>> $INSTALL_LOG
        apt-get install -yq spotify-client &>> $INSTALL_LOG
    else
        echo "spotify-client installed"
        echo "spotify-client installed" &>> $INSTALL_LOG
    fi

    echo "Upgrading Installed Packages"
    if !(apt-get -yq upgrade &>> $INSTALL_LOG); then
    	echo "Program Installation Failed"
    	exit 1
    fi

    if !(ldconfig &>> $INSTALL_LOG); then
    	echo "ldconfig failure"
    	exit 1
    fi

fi





#=========================================================================
# 3) Install dotfiles/RSAID/VIM Plugins
#=========================================================================
if [[ $prompt_install_dotfiles =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Installing dotfiles"
    cd $INIT_DIR &>> $INSTALL_LOG
    sudo -u $SUDO_USER bash install_dotfiles.sh &>> $INSTALL_LOG
    source $HOME/.bashrc &>> $INSTALL_LOG

fi


if [[ $prompt_install_rsaid =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Installing RSAID" &>> $INSTALL_LOG
    cd $INIT_DIR &>> $INSTALL_LOG
    sudo -u $SUDO_USER bash install_rsaid.sh

fi

if [[ $prompt_install_vimplugins =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Installing VIM Plugins"
    cd $INIT_DIR &>> $INSTALL_LOG
    sudo -u $SUDO_USER bash install_vimplugins.sh &>> $INSTALL_LOG

fi





#=========================================================================
# 4) Install software repositories
#=========================================================================
if [[ $prompt_install_softwarerepos =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Installing PeRLs"
    echo "Installing PeRLs" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/perls/trunk perls &>> $INSTALL_LOG
    cd perls/third-party &>> $INSTALL_LOG
    sudo -u $SUDO_USER yes | ./perls-essential.sh &>> $INSTALL_LOG
    cd build &>> $INSTALL_LOG
    sudo -u $SUDO_USER cmake ../ &>> $INSTALL_LOG
    sudo -u $SUDO_USER make &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

    echo "Installing april"
    echo "Installing april" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git://april.eecs.umich.edu/home/git/april.git &>> $INSTALL_LOG
    cd $HOME/april/src &>> $INSTALL_LOG
    sudo -u $SUDO_USER make &>> $INSTALL_LOG
    cd $HOME/april/java &>> $INSTALL_LOG
    sudo -u $SUDO_USER env CLASSPATH=$CLASSPATH:/usr/share/java/gluegen-rt.jar:/usr/local/share/java/lcm.jar:$HOME/april/java/april.jar:./ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/april/lib ant &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

    echo "Installing ngv"
    echo "Installing ngv" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone ngv@atlas:~ngv/ngv.git &>> $INSTALL_LOG
    cd ngv &>> $INSTALL_LOG
    sudo -u $SUDO_USER git remote rename origin dist &>> $INSTALL_LOG
    sudo -u $SUDO_USER git remote add origin atlas:~rwolcott/ngv.git &>> $INSTALL_LOG
    cd src &>> $INSTALL_LOG
    sudo -u $SUDO_USER env CLASSPATH=$CLASSPATH:/usr/share/java/gluegen-rt.jar:/usr/local/share/java/lcm.jar:$HOME/april/java/april.jar:./ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/april/lib make &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

    echo "Installing dngv"
    echo "Installing dngv" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone ngv@atlas:~ngv/dngv.git &>> $INSTALL_LOG
    cd dngv &>> $INSTALL_LOG
    sudo -u $SUDO_USER git remote rename origin dist &>> $INSTALL_LOG
    sudo -u $SUDO_USER git remote add origin atlas:~rwolcott/dngv.git &>> $INSTALL_LOG
    cd java &>> $INSTALL_LOG
    sudo -u $SUDO_USER env CLASSPATH=$CLASSPATH:/usr/share/java/gluegen-rt.jar:/usr/local/share/java/lcm.jar:$HOME/april/java/april.jar:$HOME/ngv/java/ngv.jar:$HOME/dngv/java/dngv.jar:./ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/april/lib ant &>> $INSTALL_LOG
    cd ../src/openjaus &>> $INSTALL_LOG
    sudo -u $SUDO_USER make &>> $INSTALL_LOG
    cd .. &>> $INSTALL_LOG
    sudo -u $SUDO_USER make &>> $INSTALL_LOG
    cd .. &>> $INSTALL_LOG
    echo "..getting data repo" &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone ngv@atlas:dngv_data.git data &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

    echo "Populating soft directory"
    echo "Populating soft directory" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER mkdir -p soft &>> $INSTALL_LOG
    cd soft &>> $INSTALL_LOG
    sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/dgc-umich dgc-umich &>> $INSTALL_LOG
    sudo -u $SUDO_USER cp -r $HOME/perls perls_clean
    sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/perls/branches/neec neec &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

fi






#=========================================================================
# 5) Configure personal repositories/settings
#=========================================================================
if [[ $prompt_install_personalrepos =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    # configure personal directories
    echo "Configuring personal directories"
    echo "Configuring personal directories" &>> $INSTALL_LOG

    cd $HOME/perls/src &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git@bitbucket.org:rwolcott/wolcott-perls.git wolcott &>> $INSTALL_LOG
    echo -e "\nperls_add_group (wolcott wolcott "Build wolcott group?" OFF)" >> CMakeLists.txt
    sudo -u $SUDO_USER ln -sf $HOME/perls/src/wolcott/config/ofl.cfg $HOME/perls/config/ofl.cfg
    sudo -u $SUDO_USER ln -sf $HOME/perls/src/wolcott/config/procman/procman-ofl.cfg $HOME/perls/config/procman/procman-ofl.cfg
    sudo -u $SUDO_USER ln -sf $HOME/perls/src/wolcott/config/viewer/ofl.cfg $HOME/perls/config/viewer/ofl.cfg

    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git@bitbucket.org:rwolcott/rawr.git &>> $INSTALL_LOG
    sudo -u $SUDO_USER ln -sf $HOME/rawr/scripts/spotify-notify/spotify-notify.py /usr/local/bin/spotify &>> $INSTALL_LOG

    echo "Installing ctags" &>> $INSTALL_LOG
    cd $INIT_DIR &>> $INSTALL_LOG
    sudo -u $SUDO_USER bash install_ctags.sh

    # configure udp receive buffer
    echo "Setting udp receive buffer"
    echo "Setting udp receive buffer" &>> $INSTALL_LOG
    if grep 'rmem_default=2097152' /etc/sysctl.conf > /dev/null 2>&1; then
        echo "rmem_default already set"
    else
        echo "Setting rmem_default for LCM"
        sysctl -w net.core.rmem_default=2097152 &>> $INSTALL_LOG
        echo 'net.core.rmem_default=2097152' >> /etc/sysctl.conf
    fi

    if grep 'rmem_max=2097152' /etc/sysctl.conf > /dev/null 2>&1; then
        echo "rmem_max already set"
    else
        echo "Setting rmem_max for LCM"
        sysctl -w net.core.rmem_max=2097152 &>> $INSTALL_LOG
        echo 'net.core.rmem_max=2097152' >> /etc/sysctl.conf
    fi

fi





#=========================================================================
# 6) Setup compiz settings
#=========================================================================
if [[ $prompt_install_compiz =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Configuring compiz.."
    echo "Configuring compiz.." > $INSTALL_LOG

    echo "apt-get -yq install compiz*"
    echo "apt-get -yq install compiz*" &>> $INSTALL_LOG
    apt-get -yq install compiz* &>> $INSTALL_LOG

    #-workspace size 3x3
    set_gconf_int /apps/compiz-1/general/screen0/options/hsize 3
    set_gconf_int /apps/compiz-1/general/screen0/options/vsize 3

    #-unity size (make smallest)
    set_gconf_int /apps/compiz-1/plugins/unityshell/screen0/options/icon_size 16

    #-unity autohide
    set_gconf_int /apps/compiz-1/plugins/unityshell/screen0/options/launcher_hide_mode 1

    #-disable unity switcher
    set_gconf_string /apps/compiz-1/plugins/unityshell/screen0/options/alt_tab_forward "Disabled"
    set_gconf_string /apps/compiz-1/plugins/unityshell/screen0/options/alt_tab_next_window "Disabled"
    set_gconf_string /apps/compiz-1/plugins/unityshell/screen0/options/alt_tab_prev "Disabled"
    set_gconf_string /apps/compiz-1/plugins/unityshell/screen0/options/alt_tab_prev_window "Disabled"

    #-compiz plugins
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --type list --list-type string --set /apps/compiz-1/general/screen0/options/active_plugins "[core,composite,opengl,decor,gnomecompat,imgpng,resize,regex,grid,vpswitch,compiztoolbox,mousepoll,place,move,snap,animation,unitymtgrabhandles,session,wall,fade,expo,workarounds,scale,ezoom,unityshell,staticswitcher]"

    #-configure staticswitcher
    set_gconf_int /apps/compiz-1/plugins/staticswitcher/screen0/options/brightness 100
    set_gconf_int /apps/compiz-1/plugins/staticswitcher/screen0/options/highlight_mode 1
    set_gconf_int /apps/compiz-1/plugins/staticswitcher/screen0/options/highlight_rect_hidden 2
    set_gconf_bool /apps/compiz-1/plugins/staticswitcher/screen0/options/icon 1
    set_gconf_bool /apps/compiz-1/plugins/staticswitcher/screen0/options/icon_only 0
    set_gconf_bool /apps/compiz-1/plugins/staticswitcher/screen0/options/minimized 1
    set_gconf_bool /apps/compiz-1/plugins/staticswitcher/screen0/options/mouse_select 1
    set_gconf_int /apps/compiz-1/plugins/staticswitcher/screen0/options/opacity 100
    set_gconf_float /apps/compiz-1/plugins/staticswitcher/screen0/options/popup_delay 0
    set_gconf_int /apps/compiz-1/plugins/staticswitcher/screen0/options/saturation 100
    set_gconf_float /apps/compiz-1/plugins/staticswitcher/screen0/options/speed 50
    set_gconf_float /apps/compiz-1/plugins/staticswitcher/screen0/options/timestap 0.1

    #-grid settings
    set_gconf_string /apps/compiz-1/plugins/grid/screen0/options/put_bottomright_key "<Shift><Control><Primary><Super>period"
    set_gconf_string /apps/compiz-1/plugins/grid/screen0/options/put_bottomleft_key "<Shift><Control><Primary><Super>m"
    set_gconf_string /apps/compiz-1/plugins/grid/screen0/options/put_topright_key "<Shift><Control><Primary><Super>o"
    set_gconf_string /apps/compiz-1/plugins/grid/screen0/options/put_topleft_key "<Shift><Control><Primary><Super>u"
    set_gconf_string /apps/compiz-1/plugins/grid/screen0/options/put_right_key "<Shift><Control><Primary><Super>l"
    set_gconf_string /apps/compiz-1/plugins/grid/screen0/options/put_left_key "<Shift><Control><Primary><Super>h"

    #-window resize override
    set_gconf_string /apps/compiz-1/plugins/core/screen0/options/window_menu_button "<Alt>Button2"

    #-window resize
    set_gconf_string /apps/compiz-1/plugins/resize/screen0/options/initiate_button "<Alt>Button3"



fi





#=========================================================================
# 6) Finished
#=========================================================================
echo -e "\nDone\n"
echo -e "\nDone\n" &>> $INSTALL_LOG



cd $INIT_DIR
