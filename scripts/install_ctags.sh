#!/bin/bash

TAG_DIR=$HOME/.vim/tags     # tag directory
START_DIR=`pwd`

echo "Building ctag files"

mkdir -p $TAG_DIR
cd $TAG_DIR
bash update_tags.sh

cd $START_DIR
