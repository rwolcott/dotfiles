#!/bin/bash
PACKAGES=""
function addpkg {
    PACKAGES="$PACKAGES $@"
}
function ask_yn() { # {prompt question}
    read -p "$1" -n 1 -r
    if [[ $REPLY != "" ]]; then
        echo
    fi
}
function set_gconf_string() { # {key, value}
    echo "gconf_string: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type string "$2" &>> $INSTALL_LOG
}
function set_gconf_bool() { # {key, value}
    echo "gconf_bool: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type bool $2 &>> $INSTALL_LOG
}
function set_gconf_float() { # {key, value}
    echo "gconf_float: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type float $2 &>> $INSTALL_LOG
}
function set_gconf_int() { # {key, value}
    echo "gconf_int: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gconftool-2 --set $1 --type int $2 &>> $INSTALL_LOG
}
function set_dconf() { # {key, value}
    echo "dconf write: <$1> <$2>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS dconf write $1 "$2" &>> $INSTALL_LOG
}
function set_gsettings() { # {schema, key, value}
    echo "gsettings set: <$1> <$2> <$3>" &>> $INSTALL_LOG
    sudo -u $SUDO_USER DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS gsettings set $1 $2 $3 &>> $INSTALL_LOG
}

if [ `id -u` -ne "0" ]; then
	echo "You must be root to run this script"
	exit 1
fi;



#================================================
# Batch prompt what to install..
#================================================


ask_yn "Would you like to install dotfiles? [Y/n] "
prompt_install_dotfiles=$REPLY
ask_yn "Would you like to install rsaid? [Y/n] "
prompt_install_rsaid=$REPLY
ask_yn "Would you like to install third-party apps? [Y/n] "
prompt_install_thirdparty=$REPLY
ask_yn "Would you like to install software repos.? [Y/n] "
prompt_install_softwarerepos=$REPLY
ask_yn "Would you like to install personal repos.? [Y/n] "
prompt_install_personalrepos=$REPLY


echo ""
echo "Installation will start in 5 seconds.."
#sleep 5

INIT_DIR=`pwd`
INSTALL_LOG=/var/tmp/setup.log
export HOME=/home/$SUDO_USER
source $HOME/.bashrc &>> $INSTALL_LOG
cd $HOME

echo ""
echo ""
echo "You may monitor a more detailed output of the setup with"
echo "       tail -f $INSTALL_LOG"
echo ""
echo ""

#=========================================================================
# 0) Obtain dbus address hack for setting gconf under sudo
#=========================================================================
PID=$(pgrep -u $SUDO_USER gnome-session)
DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ | cut -d '=' -f 2-)



#=========================================================================
# 1) Install dotfiles/RSAID
#=========================================================================
if [[ $prompt_install_dotfiles =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Installing dotfiles"
    cd $INIT_DIR &>> $INSTALL_LOG
    sudo -u $SUDO_USER bash install_dotfiles.sh &>> $INSTALL_LOG
    source $HOME/.bashrc &>> $INSTALL_LOG

fi


if [[ $prompt_install_rsaid =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Installing RSAID" &>> $INSTALL_LOG
    cd $INIT_DIR &>> $INSTALL_LOG
    sudo -u $SUDO_USER bash install_rsaid.sh

fi




# LEGACY:
##=========================================================================
## 2) Setup shortcuts and configs
##=========================================================================
#if [[ $prompt_install_shortcuts =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then
#
#    echo "Setting up shortcuts and configs.."
#    echo "Setting up shortcuts and configs.." > $INSTALL_LOG
#
#    #-fonts
#    #set_gconf_string /desktop/gnome/interface/document_font_name "Sans 10"
#    #set_gconf_string /desktop/gnome/interface/monospace_font_name "Ubuntu Mono 11"
#    #set_gconf_string /desktop/gnome/interface/font_name "Ubuntu 10"
#    #set_gconf_string /apps/metacity/general/titlebar_font "Ubuntu Bold 10"
#    #set_gconf_string /apps/nautilus/preferences/desktop_font "Ubuntu 11"
#
#    set_dconf /org/gnome/desktop/interface/document-font-name "'Sans 10'"
#    set_dconf /org/gnome/desktop/interface/monospace-font-name "'Ubuntu Mono 11'"
#    set_dconf /org/gnome/desktop/interface/font-name "'Ubuntu 10'"
#    set_dconf /org/gnome/desktop/wm/preferences/titlebar-font "'Ubuntu Bold 10'"
#    set_dconf /org/gnome/nautilus/desktop/font "'Ubuntu 11'"
#
#    #-terminal settings
#    set_gconf_bool /apps/gnome-terminal/profiles/Default/scrollback_unlimited 1
#    set_gconf_bool /apps/gnome-terminal/profiles/Default/use_system_font 0
#    set_gconf_string /apps/gnome-terminal/profiles/Default/font "Ubuntu Mono derivative Powerline 9"
#    set_gconf_float /apps/gnome-terminal/profiles/Default/background_darkness 1
#    set_gconf_string /apps/gnome-terminal/profiles/Default/background_type "solid"
#    set_gconf_bool /apps/gnome-terminal/profiles/Default/use_theme_colors 0
#    set_gconf_string /apps/gnome-terminal/profiles/Default/palette "#2E2E34343636:#CCCC00000000:#4E4E9A9A0606:#C4C4A0A00000:#34346565A4A4:#757550507B7B:#060698209A9A:#D3D3D7D7CFCF:#555557575353:#EFEF29292929:#8A8AE2E23434:#FCFCE9E94F4F:#72729F9FCFCF:#ADAD7F7FA8A8:#3434E2E2E2E2:#EEEEEEEEECEC"
#    set_gconf_string /apps/gnome-terminal/profiles/Default/background_color "#000000000000"
#    set_gconf_string /apps/gnome-terminal/profiles/Default/foreground_color "#FFFFFFFFFFFF"
#
#    #-default browser
#    set_gconf_string /desktop/gnome/applications/browser/exec "/opt/google/chrome/google-chrome"
#
#    # indicator area settings
#    set_dconf /com/canonical/indicator/power/show-time true
#    set_dconf /com/canonical/indicator/keyboard/visible false
#    set_dconf /com/canonical/indicator/datetime/show-clock true
#    set_dconf /com/canonical/indicator/datetime/show-date true
#    set_dconf /com/canonical/indicator/datetime/show-day true
#    set_dconf /com/canonical/indicator/datetime/show-seconds true
#    set_dconf /apps/indicators/multiload/autostart true
#    set_dconf /apps/indicators/multiload/graphs/cpu/enabled true
#    set_dconf /apps/indicators/multiload/graphs/mem/enabled true
#    set_dconf /apps/indicators/multiload/graphs/net/enabled true
#    set_dconf /apps/indicators/multiload/graphs/load/enabled true
#    set_dconf /apps/indicators/multiload/graphs/disk/enabled true
#
#    #-workspace movement (ctrl+shift+[HJKL])
#    set_dconf /org/gnome/desktop/wm/keybindings/switch-to-workspace-up "['<Control><Shift>k']"
#    set_dconf /org/gnome/desktop/wm/keybindings/switch-to-workspace-right "['<Control><Shift>l']"
#    set_dconf /org/gnome/desktop/wm/keybindings/switch-to-workspace-left "['<Control><Shift>h']"
#    set_dconf /org/gnome/desktop/wm/keybindings/switch-to-workspace-down "['<Control><Shift>j']"
#
#    #-workspace window movement (ctrl+shift+alt+[HJKL])
#    set_dconf /org/gnome/desktop/wm/keybindings/move-to-workspace-up "['<Control><Shift><Alt>k']"
#    set_dconf /org/gnome/desktop/wm/keybindings/move-to-workspace-right "['<Control><Shift><Alt>l']"
#    set_dconf /org/gnome/desktop/wm/keybindings/move-to-workspace-left "['<Control><Shift><Alt>h']"
#    set_dconf /org/gnome/desktop/wm/keybindings/move-to-workspace-down "['<Control><Shift><Alt>j']"
#
#    #-new terminal (ctrl+shift+X)
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/terminal "'<Control><Shift>x'"
#
#
#    #-toggle maximization (ctrl+shift+Z)
#    set_dconf /org/gnome/desktop/wm/keybindings/toggle-maximized "['<Control><Shift>z']"
#
#    #-media key redirects using xdotool
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/','/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/','/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/','/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/','/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/']"
#
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/name "'Volume Down Ryan'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/command "'xdotool key --clearmodifiers --delay 0 XF86AudioLowerVolume'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/binding "'<Control><Shift>u'"
#
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/name "'Volume Up Ryan'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/command "'xdotool key --clearmodifiers --delay 0 XF86AudioRaiseVolume'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/binding "'<Control><Shift>i'"
#
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/name "'Next Track Ryan'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/command "'xdotool key --clearmodifiers --delay 0 XF86AudioNext'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/binding "'<Control><Shift>o'"
#
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/name "'Previous Track Ryan'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/command "'xdotool key --clearmodifiers --delay 0 XF86AudioPrev'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/binding "'<Control><Shift>y'"
#
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/name "'Play/Pause Ryan'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/command "'xdotool key --clearmodifiers --delay 0 XF86AudioPlay'"
#    set_dconf /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/binding "'<Control><Shift>p'"
#
#    #-remove user name from panel
#    #gsettings set com.canonical.indicator.session show-real-name-on-panel false &>> $INSTALL_LOG
#
#fi
#
#
#
#
#
##=========================================================================
## 3) Setup compiz settings
##=========================================================================
#if [[ $prompt_install_compiz =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then
#
#    echo "Configuring compiz.."
#    echo "Configuring compiz.." > $INSTALL_LOG
#
#    echo "apt-get -yq install compiz compizconfig-settings-manager compiz-plugins*"
#    echo "apt-get -yq install compiz compizconfig-settings-manager compiz-plugins*" &>> $INSTALL_LOG
#    apt-get -yq install compiz compizconfig-settings-manager compiz-plugins* &>> $INSTALL_LOG
#
#    #-workspace size 3x3
#    set_dconf /org/compiz/profiles/unity/plugins/core/hsize 3
#    set_dconf /org/compiz/profiles/unity/plugins/core/vsize 3
#
#    #-unity size (make smallest)
#    set_dconf /org/compiz/profiles/unity/plugins/unityshell/icon-size 16
#
#    #-unity autohide
#    set_dconf /org/compiz/profiles/unity/plugins/unityshell/launcher-hide-mode 1
#
#    #-disable unity switcher
#    set_dconf /org/compiz/profiles/unity/plugins/unityshell/alt-tab-forward "'Disabled'"
#    set_dconf /org/compiz/profiles/unity/plugins/unityshell/alt-tab-next-window "'Disabled'"
#    set_dconf /org/compiz/profiles/unity/plugins/unityshell/alt-tab-prev "'Disabled'"
#    set_dconf /org/compiz/profiles/unity/plugins/unityshell/alt-tab-prev-window "'Disabled'"
#
#    #-disable unity shortcuts
#    set_dconf /org/compiz/profiles/unity/plugins/unityshell/show-menu-bar "'Disabled'"
#    set_dconf /org/compiz/integrated/show-hud "['disabled']"
#
#    #-compiz plugins
#    set_dconf /org/compiz/profiles/unity/plugins/core/active-plugins "['core','composite','opengl','decor','gnomecompat','imgpng','resize','regex','grid','vpswitch','compiztoolbox','mousepoll','place','move','snap','animation','unitymtgrabhandles','session','wall','fade','expo','workarounds','scale','ezoom','unityshell','staticswitcher']"
#
#    #-configure staticswitcher
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/saturation 100
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/brightness 100
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/opacity 40
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/highlight-mode 1
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/highlight-rect-hidden 2
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/icon true
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/icon-only false
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/focus-on-switch true
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/bring-to-front true
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/highlight-color "'#00000000'"
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/highlight-border-color "'#000000c5'"
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/highlight-border-inlay-color "'#f88659c5'"
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/speed 50
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/timestep 0.1
#    set_dconf /org/compiz/profiles/unity/plugins/staticswitcher/popup-delay 0.0
#    set_dconf /org/gnome/desktop/wm/keybindings/switch-windows "['<Alt>Tab']"
#    set_dconf /org/gnome/desktop/wm/keybindings/switch-windows-backward "['<Shift><Alt>Tab']"
#
#    #-grid settings
#    set_dconf /org/compiz/profiles/unity/plugins/grid/put-bottomright-key "'<Shift><Control><Super>greater'"
#    set_dconf /org/compiz/profiles/unity/plugins/grid/put-bottomleft-key "'<Shift><Control><Super>m'"
#    set_dconf /org/compiz/profiles/unity/plugins/grid/put-topright-key "'<Shift><Control><Super>o'"
#    set_dconf /org/compiz/profiles/unity/plugins/grid/put-topleft-key "'<Shift><Control><Super>u'"
#    set_dconf /org/compiz/profiles/unity/plugins/grid/put-right-key "'<Shift><Control><Super>l'"
#    set_dconf /org/compiz/profiles/unity/plugins/grid/put-left-key "'<Shift><Control><Super>h'"
#
#    #-window resize override
#    set_dconf /org/compiz/profiles/unity/plugins/core/window-menu-button "'<Alt>Button2'"
#
#    #-window resize
#    set_dconf /org/compiz/profiles/unity/plugins/resize/initiate-button "'<Alt>Button3'"
#    set_dconf /org/gnome/desktop/wm/preferences/resize-with-right-button true
#
#fi










#=========================================================================
# 4) Install 3rd party apps
#=========================================================================

if [[ $prompt_install_thirdparty =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Updating System"
    echo "Updating System" > $INSTALL_LOG

    # Add extra repositories
    add-apt-repository -y ppa:pithos/ppa &>> $INSTALL_LOG
    add-apt-repository -y ppa:synapse-core/testing &>> $INSTALL_LOG

    # First, lets get up-to-date
    if !(apt-get -yq update &>> $INSTALL_LOG); then
        echo "apt-get update failure"
        exit 1
    fi;

    apt-get install -yq python-software-properties &>> $INSTALL_LOG

    # Personal packages
    addpkg \
        pithos \
        synapse \
        vlc \
        vim-gnome \
        subversion \
        ssh \
        openssh-server \
        git-core \
        gitk \
        gitg \
        ant \
        gtk-doc-tools \
        libglib2.0-dev \
        libusb-1.0-0-dev \
        gv \
        libncurses-dev \
        libncurses5-dev \
        autopoint \
        libgl1-mesa-dev \
        socat \
        ctags \
        inkscape \
        virtualbox \
        openssh-client \
        openssh-server \
        pkg-config \
        scons \
        sysstat \
        make \
        ruby \
        python \
        python-indicate \
        python-notify \
        apt-file \
        colormake \
        indicator-multiload \
        indicator-cpufreq \
        valgrind \
        kcachegrind \
        meld \
        xdotool \
        minidlna \
        gconf-editor \
        screen \
        pv \
        autossh \
        libeigen3-dev \
        libsuitesparse-dev \
        ethtool \
        terminator \
        tmux \
        tinyproxy \
        ruby-ronn \
        python-pip \
        libdc1394-22-dev \
        libgtk2.0-dev \
        libgps-dev \
        libqhull-dev \
        libf2c2-dev \
        libboost-all-dev \
        libgsl0-dev \
        ptpd \
        espeak \
        libncurses5-dev \
        libudev-dev \
        libjpeg-dev \
        libssl-dev \
        acpi \
        scrot \
        corkscrew \
        libglew-dev \
        libatlas-dev \
        feh \
        trash-cli \
        rxvt-unicode-256color \
        fonts-font-awesome \
        libav-tools \
        i3-wm \
        i3blocks \
        spotify-client \
        zsh \
        dunst \
        gcalcli



    echo "Installing essential packages"
    echo "Installing essential packages" &>> $INSTALL_LOG
    # General installations
    echo "apt-get install $PACKAGES"
    echo "apt-get install $PACKAGES" &>> $INSTALL_LOG
    apt-get update &>> $INSTALL_LOG
    apt-get -yq install $PACKAGES &>> $INSTALL_LOG || \
        { \
            echo "Program Installation Failed"; \
            exit 1; \
        }

    # additional apps that require downloaded deb
    # GOOGLE CHROME
    if ! which google-chrome > /dev/null 2>&1; then
        echo "Installing google-chrome"
        echo "Installing google-chrome" >> $INSTALL_LOG
        wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb &>> $INSTALL_LOG
        if [ -e "google-chrome-stable_current_amd64.deb" ]; then
            echo "google-chrome download finished"
            echo "google-chrome download finished" &>> $INSTALL_LOG
            dpkg -i google-chrome-stable_current_amd64.deb &>> $INSTALL_LOG
            apt-get -f install &>> $INSTALL_LOG
            rm google-chrome-stable_current_amd64.deb &>> $INSTALL_LOG
        else
            echo "google-chrome didn't download"
            echo "google-chrome didn't download" &>> $INSTALL_LOG
            exit 1;
        fi else echo "google-chrome installed"
        echo "google-chrome installed" &>> $INSTALL_LOG
    fi

#    # MENDELEY
#    if ! which mendeleydesktop > /dev/null 2>&1; then
#        echo "Installing mendeley"
#        echo "Installing mendeley" >> $INSTALL_LOG
#        wget http://www.mendeley.com/repositories/ubuntu/stable/amd64/mendeleydesktop-latest &>> $INSTALL_LOG
#        if [ -e "mendeleydesktop-latest" ]; then
#            echo "mendeleydesktop download finished"
#            echo "mendeleydesktop download finished" &>> $INSTALL_LOG
#            dpkg -i mendeleydesktop-latest &>> $INSTALL_LOG
#            rm mendeleydesktop-latest &>> $INSTALL_LOG
#        else
#            echo "mendeleydesktop didn't download"
#            echo "mendeleydesktop didn't download" &>> $INSTALL_LOG
#            exit 1;
#        fi
#    else
#        echo "mendeleydesktop installed"
#        echo "mendeleydesktop installed" &>> $INSTALL_LOG
#    fi

    # SPOTIFY
    if ! which spotify > /dev/null 2>&1; then
        echo "Installing spotify"
        echo "Installing spotify" >> $INSTALL_LOG
        echo "deb http://repository.spotify.com stable non-free" >> /tmp/spotify-client.list
        cp /tmp/spotify-client.list /etc/apt/sources.list.d/
        rm /tmp/spotify-client.list
        apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys D2C19886 &>> $INSTALL_LOG
        apt-get update &>> $INSTALL_LOG
        apt-get install -yq spotify-client &>> $INSTALL_LOG
    else
        echo "spotify-client installed"
        echo "spotify-client installed" &>> $INSTALL_LOG
    fi

    echo "Upgrading Installed Packages"
    if !(apt-get -yq upgrade &>> $INSTALL_LOG); then
    	echo "Program Installation Failed"
    	exit 1
    fi

    if !(ldconfig &>> $INSTALL_LOG); then
    	echo "ldconfig failure"
    	exit 1
    fi

fi





#=========================================================================
# 5) Install software repositories
#=========================================================================
if [[ $prompt_install_softwarerepos =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    echo "Populating soft directory"
    echo "Populating soft directory" &>> $INSTALL_LOG

    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER mkdir -p soft &>> $INSTALL_LOG
    cd soft &>> $INSTALL_LOG

    echo "..LCM"
    echo "..LCM" &>> $INSTALL_LOG
    cd $HOME/soft &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone https://code.google.com/p/lcm/ &>> $INSTALL_LOG
    cd lcm &>> $INSTALL_LOG
    sudo -u $SUDO_USER ./bootstrap.sh &>> $INSTALL_LOG
    sudo -u $SUDO_USER ./configure &>> $INSTALL_LOG
    sudo -u $SUDO_USER make &>> $INSTALL_LOG
    make install &>> $INSTALL_LOG
    cd $HOME/soft &>> $INSTALL_LOG

    echo "..dgc"
    echo "..dgc" &>> $INSTALL_LOG
    cd $HOME/soft &>> $INSTALL_LOG
    #sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/dgc-umich dgc-umich &>> $INSTALL_LOG
    sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/dgc-umich/software/ford dgc-ford &>> $INSTALL_LOG
    sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/dgc-umich/software/trunk dgc-mit &>> $INSTALL_LOG
    cd $HOME/soft &>> $INSTALL_LOG

    # DO THIS YOURSELF:
    #echo "..CUDA"
    #echo "..CUDA" &>> $INSTALL_LOG
    #echo "..OpenCV"
    #echo "..OpenCV" &>> $INSTALL_LOG
    #wget http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.9/opencv-2.4.9.zip
    #unzip opencv-2.4.9.zip
    #cd opencv-2.4.9
    #mkdir build
    #cd build
    # user: CUDA, OpenCV, PCL

    #old
    #sudo -u $SUDO_USER cp -r $HOME/perls perls_clean
    #sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/perls/branches/neec neec &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG


    echo "Checking out PeRLs"
    echo "Checking out PeRLs" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER svn co --non-interactive --trust-server-cert svn+ssh://rwolcott@robots.engin.umich.edu/var/svn/software/perls/trunk perls &>> $INSTALL_LOG
    #cd perls/third-party &>> $INSTALL_LOG
    #sudo -u $SUDO_USER yes | ./perls-essential.sh &>> $INSTALL_LOG
    #cd build &>> $INSTALL_LOG
    #sudo -u $SUDO_USER cmake ../ &>> $INSTALL_LOG
    #sudo -u $SUDO_USER make &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

    echo "Checking out april"
    echo "Checking out april" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git://april.eecs.umich.edu/home/git/april.git &>> $INSTALL_LOG
    #cd $HOME/april/src &>> $INSTALL_LOG
    #sudo -u $SUDO_USER make &>> $INSTALL_LOG
    #cd $HOME/april/java &>> $INSTALL_LOG
    #sudo -u $SUDO_USER env CLASSPATH=$CLASSPATH:/usr/share/java/gluegen-rt.jar:/usr/local/share/java/lcm.jar:$HOME/april/java/april.jar:./ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/april/lib ant &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

#    echo "Checking out ngv"
#    echo "Checking out ngv" &>> $INSTALL_LOG
#    cd $HOME &>> $INSTALL_LOG
#    sudo -u $SUDO_USER git clone ngv@atlas:~ngv/ngv.git &>> $INSTALL_LOG
#    cd ngv &>> $INSTALL_LOG
#    sudo -u $SUDO_USER git remote rename origin dist &>> $INSTALL_LOG
#    sudo -u $SUDO_USER git remote add origin atlas:~rwolcott/ngv.git &>> $INSTALL_LOG
#    #cd src &>> $INSTALL_LOG
#    #sudo -u $SUDO_USER env CLASSPATH=$CLASSPATH:/usr/share/java/gluegen-rt.jar:/usr/local/share/java/lcm.jar:$HOME/april/java/april.jar:./ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/april/lib make &>> $INSTALL_LOG
#    cd $HOME &>> $INSTALL_LOG

    echo "Checking out dngv"
    echo "Checking out dngv" &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git@atlas.ngv.engin.umich.edu:root/dngv.git &>> $INSTALL_LOG
    cd dngv &>> $INSTALL_LOG
    sudo -u $SUDO_USER git remote rename origin dist &>> $INSTALL_LOG
    sudo -u $SUDO_USER git remote add origin atlas:~rwolcott/dngv.git &>> $INSTALL_LOG
    #cd java &>> $INSTALL_LOG
    #sudo -u $SUDO_USER env CLASSPATH=$CLASSPATH:/usr/share/java/gluegen-rt.jar:/usr/local/share/java/lcm.jar:$HOME/april/java/april.jar:$HOME/ngv/java/ngv.jar:$HOME/dngv/java/dngv.jar:./ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/april/lib ant &>> $INSTALL_LOG
    #cd ../src/openjaus &>> $INSTALL_LOG
    #sudo -u $SUDO_USER make &>> $INSTALL_LOG
    #cd .. &>> $INSTALL_LOG
    #sudo -u $SUDO_USER make &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

    echo "..cloning data repo (this may take a while)"
    echo "..cloning data repo (this may take a while)" &>> $INSTALL_LOG
    cd $HOME/dngv &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone ngv@atlas:dngv_data_new.git data &>> $INSTALL_LOG
    cd $HOME &>> $INSTALL_LOG

fi






#=========================================================================
# 6) Configure personal repositories/settings
#=========================================================================
if [[ $prompt_install_personalrepos =~ ^([Yy][eE][sS]|[yY]|"")$ ]]; then

    # configure personal directories
    echo "Configuring personal directories"
    echo "Configuring personal directories" &>> $INSTALL_LOG

    cd $HOME/perls/src &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git@bitbucket.org:rwolcott/wolcott-perls.git wolcott &>> $INSTALL_LOG
    echo -e "\nperls_add_group (wolcott wolcott "Build wolcott group?" OFF)" >> CMakeLists.txt
    sudo -u $SUDO_USER ln -sf $HOME/perls/src/wolcott/config/ofl.cfg $HOME/perls/config/ofl.cfg
    sudo -u $SUDO_USER ln -sf $HOME/perls/src/wolcott/config/procman/procman-ofl.cfg $HOME/perls/config/procman/procman-ofl.cfg
    sudo -u $SUDO_USER ln -sf $HOME/perls/src/wolcott/config/viewer/ofl.cfg $HOME/perls/config/viewer/ofl.cfg

    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git@bitbucket.org:rwolcott/visloc &>> $INSTALL_LOG

    cd $HOME &>> $INSTALL_LOG
    sudo -u $SUDO_USER git clone git@bitbucket.org:rwolcott/rawr.git &>> $INSTALL_LOG
    ln -sf $HOME/rawr/scripts/spotify-notify/spotify-notify.py /usr/local/bin/spotify &>> $INSTALL_LOG

    echo "Installing ctags" &>> $INSTALL_LOG
    cd $INIT_DIR &>> $INSTALL_LOG
    sudo -u $SUDO_USER bash install_ctags.sh

    # configure udp receive buffer
    echo "Setting udp receive buffer"
    echo "Setting udp receive buffer" &>> $INSTALL_LOG
    if grep 'rmem_default=2097152' /etc/sysctl.conf > /dev/null 2>&1; then
        echo "rmem_default already set"
    else
        echo "Setting rmem_default for LCM"
        sysctl -w net.core.rmem_default=2097152 &>> $INSTALL_LOG
        echo 'net.core.rmem_default=2097152' >> /etc/sysctl.conf
    fi

    if grep 'rmem_max=2097152' /etc/sysctl.conf > /dev/null 2>&1; then
        echo "rmem_max already set"
    else
        echo "Setting rmem_max for LCM"
        sysctl -w net.core.rmem_max=2097152 &>> $INSTALL_LOG
        echo 'net.core.rmem_max=2097152' >> /etc/sysctl.conf
    fi

    # configure for autossh
    echo "Configuring autossh for gitlab"
    echo "Configuring autossh for gitlab" &>> $INSTALL_LOG
    cp $HOME/dotfiles/misc/autossh/autossh.conf /etc/init/. &>> $INSTALL_LOG
    cp $HOME/dotfiles/misc/autossh/autossh_host.conf /etc/init/. &>> $INSTALL_LOG
    echo "-NL 12345:127.0.0.1:12345 -o StrictHostKeyChecking=no -i /home/${SUDO_USER}/.ssh/id_rsa rwolcott@atlas.ngv.engin.umich.edu" > /etc/autossh.hosts
    echo "-NL 2222:127.0.0.1:2222 -o StrictHostKeyChecking=no -i /home/${SUDO_USER}/.ssh/id_rsa rwolcott@atlas.ngv.engin.umich.edu" >> /etc/autossh.hosts
    restart autossh &>> $INSTALL_LOG

fi

# git clone https://github.com/vivien/i3blocks.git
# cd i3blocks
# make
# sudo make install
# sudo pip install i3-py




#=========================================================================
# 7) Finished
#=========================================================================
echo -e "\nDone\n"
echo -e "\nDone\n" &>> $INSTALL_LOG



cd $INIT_DIR

#It looks like OP solved the problem, but for anyone else who needed to fix it (myself included), just run:
#
#sudo usermod -g users `whoami`
#which pm-suspend || sudo apt-get install pm-utils #make sure you have pm-suspend command, install it if you don't
#run sudo visudo and add:
#
#%users ALL = NOPASSWD: /usr/sbin/pm-suspend
#and then add:
#
#bindsym $mod+p exec "sudo pm-suspend | i3lock"
#to ~/.i3/config. Running $mod+p will now lock and then suspend your computer.
