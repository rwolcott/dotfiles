import os
HOMEDIR = os.getenv('HOME')
# If you ever have problems with a file (noticing false-positives or
# false-negatives), debug by running
#
# $ clang++-7 -c $(python path-to-this-file) file-youre-having-trouble-with.cc
#
def FlagsForFile(filename, **kwargs):
  # Note that the standard library include directories can be obtained with:
  #   $ echo | clang++-7 -v -E -x c++ -std=c++14 -
  flags = {
    'flags': [ '-x', 'c++', '-std=c++14', '-Wall', '-fopenmp',
               # everything in 'src' (including 'third-party')
               '-I%s/driving/src' % HOMEDIR,
               # boost
               '-DBOOST_ALL_DYN_LINK',
               '-DBOOST_MPL_CFG_NO_PREPROCESSED_HEADERS',
               '-DBOOST_MPL_LIMIT_VECTOR_SIZE=50',
               '-DBOOST_ASIO_ENABLE_OLD_SERVICES',
               '-DBOOST_ASIO_DISABLE_HANDLER_TYPE_REQUIREMENTS',
               '-isystem', '%s/driving/src/bazel-src/external/boost/boost_1_70_0' % HOMEDIR,
               # tdl
               '-I%s/driving/src/bazel-src/external/tdl' % HOMEDIR,
               # tdl third-party (sophus)
               '-I%s/driving/src/bazel-src/external/tdl/third_party/sophus/Sophus' % HOMEDIR,
               # tdl third-party (folly)
               '-I%s/driving/src/bazel-src/external/tdl/third_party/folly' % HOMEDIR,
               # drake
               '-I%s/driving/src/bazel-src/external/drake' % HOMEDIR,
               # galeru
               '-I%s/driving/src/bazel-src/external' % HOMEDIR,
               # eigen
               #'-isystem', '%s/driving/src/bazel-src/external/eigen' % HOMEDIR,
               '-isystem', '%s/driving/src/bazel-src/external/eigen/eigen-eigen-323c052e1731' % HOMEDIR,
               # ceres
               '-isystem', '%s/driving/src/bazel-src/external/ceres/include' % HOMEDIR,
               # flann
               '-isystem', '%s/driving/src/bazel-src/external/flann/src/cpp' % HOMEDIR,
               # google protobuf
               '-isystem', '%s/driving/src/bazel-src/external/com_google_protobuf/src' % HOMEDIR,
               # standard c++ library
               '-isystem', '/usr/bin/../lib/gcc/x86_64-linux-gnu/5.4.0/../../../../include/c++/5.4.0',
               '-isystem', '/usr/bin/../lib/gcc/x86_64-linux-gnu/5.4.0/../../../../include/x86_64-linux-gnu/c++/5.4.0',
               '-isystem', '/usr/bin/../lib/gcc/x86_64-linux-gnu/5.4.0/../../../../include/c++/5.4.0/backward',
               '-isystem', '/usr/include/clang/7.0.0/include',

               # system-wide includes
               '-isystem', '/usr/local/include',
               '-isystem', '/usr/include/x86_64-linux-gnu',
               '-isystem', '/usr/include',
               '-isystem', '/usr/include/python3.5m',
    ],
  }
  # Most people include specific headers, but some include
  # e.g. <opencv2/opencv.h>.
  flags['flags'].extend(['-isystem', '%s/driving/src/bazel-src/external/opencv/include' % HOMEDIR])
  # OpenCV includes are in a bunch of directories:
  for dirpath, dirnames, _ in os.walk('%s/driving/src/bazel-src/external/opencv/modules' % HOMEDIR):
    if 'include' in dirnames:
      flags['flags'].extend(['-isystem', os.path.join(dirpath, 'include')])
  # OpenCV also includes a cmake-generated header that doesn't exist in
  # our bazel-based project.
  flags['flags'].extend(['-isystem', '%s/workaround-include' % os.path.dirname(os.path.realpath(__file__))])
  # QT is kind of a pain. We need to include generated files...
  flags['flags'].extend(['-I%s/driving/src/bazel-genfiles/' % HOMEDIR])
  # ...and a bunch of qt5 dirs.
  for dirpath, dirnames, _ in os.walk('/usr/include/x86_64-linux-gnu/qt5'):
    flags['flags'].extend(['-isystem', dirpath])
    for dirname in dirnames:
      flags['flags'].extend(['-isystem', os.path.join(dirpath, dirname)])
    break
  return flags
if __name__ == '__main__':
  print(' '.join(FlagsForFile('.')['flags']))

