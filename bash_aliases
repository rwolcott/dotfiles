# ALIASES
# default to tmux vim
#alias vim='$HOME/.tmux/tmux-vim'

# xbacklight -set 50
# xbacklight -dec 50
# xbacklight -inc 50

# start vim in server mode
alias vim='vim --servername VIM'
alias vi='vim --servername VIM'

alias bazel='bazel --max_idle_secs=0'
alias bb='bazel build --explain=/tmp/bazel_explain_log --verbose_explanations -s //...'

# for modifying/reloading bashrc
alias modrc='vim ~/.bashrc'
alias runrc='source ~/.bashrc'
alias addalias='vim ~/.bash_aliases'

alias playback='dgc_playback -sc'

alias spotify-online='ssh -N -D localhost:1060 ryan@localhost'

# cd aliases
alias dots='cd $DOT_PATH'
alias desktop='cd $DESKTOP_PATH'
alias docs='cd $DOC_PATH'
alias perls='cd $PERLS_PATH'
alias rawr='cd $RAWR_PATH'
alias ngv='cd $NGV_PATH'
alias dngv='cd $DNGV_PATH'
alias bin='cd $PERLS_PATH/build/bin'

alias nonet="sudo ifconfig eth0 192.168.99.99;sudo route add default dev eth0"
alias lcmlocal="export LCM_DEFAULT_URL=udpm://239.255.76.67:7667?ttl=0"
alias lcmnet="export LCM_DEFAULT_URL=udpm://239.255.76.67:7667?ttl=1"
alias lcm-logplayer-gui='lcm-logplayer-gui -p'
alias llp='lcm-logplayer-gui'
alias cleanlcmshm="rm -rf /dev/shm/lcm*"

alias tp="trash-put"
alias tl="trash-list"
alias rm="trash-put"

alias mat='matlab -nosplash -nodesktop'

alias tunnelwatch="watch -n 0.1 'ps aux | grep ssh; netstat -lpnt | grep 12345'"

alias trimount='sudo /bin/mount -t nfs -o nfsvers=3 10.120.20.20:/export/driving_data /media/nas_data'
alias tri_open='ssh robots -R 65432:localhost:22'
alias tri_vnc="ssh -t -L 5900:localhost:5900 ryan@10.120.20.43 'sudo x11vnc -display :0 -noxdamage'"
alias tri_ssh_link='ssh robots -L 23456:localhost:65432'
alias tri_ssh='ssh -p 23456 ryan@localhost'
alias tri_vnc_ssh="ssh -t -L 5900:localhost:5900 ryan@localhost -p 23456 'sudo x11vnc -display :0 -noxdamage'"

# alias makec=colormake

alias latexmk='latexmk -pdf -pvc'
#alias latexmk='latexmk -pdf'

alias lcm-spy='java lcm.spy.Spy'
#alias fordproxy='export http_proxy=http://proxyvipfmcc.nb.ford.com:83; export https_proxy=https://proxyvipfmcc.nb.ford.com:83; '
#alias localproxy='export http_proxy=127.0.0.1:8888; export https_proxy=127.0.0.1:8888;'
#export http_proxy=127.0.0.1:8888; export https_proxy=127.0.0.1:8888;

alias setbg='exec feh --bg-fill $HOME/Pictures/bg'



alias gdb='gdb -ex run --args'

# useful notify-send commands
alias notify-done='notify-send "Task Finished"'
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

head_and_tail() {
    head $1 && tail $1
}

# export inkscape
function export_inkscape() { inkscape -z -D --file="$@" --export-pdf="$@".pdf --export-latex;}
alias expink='export_inkscape'

function cd-make-dir {
    local dir olddir
    dir=$(pwd)
    olddir=$OLDPWD
    while [ "`pwd`" != "/" ]; do
        if [ -e Makefile ]; then
            break
        fi

        if [ -d build ]; then
            cd build
            break
        fi

        cd ../
    done

    if [ "`pwd`" == "/" ]; then
        cd $dir
        OLDPWD=$olddir
    else
        OLDPWD=$olddir
    fi
}

function make () {
    local dir olddir
    dir=$(pwd)
    olddir=$OLDPWD
    cd-make-dir

    echo "make $@ -C `pwd`"
    if [ -d CMakeFiles ]; then # don't colorize if cmake provided
        /usr/bin/make $@
    #else
        #/usr/bin/colormake $@
    fi
    echo $?

    cd $dir
    OLDPWD=$olddir
}

_gitg() {
    local cur opts

    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    local=$(git branch --list)
    remotes=$(git branch --remotes)
    opts="${local} ${remotes}"
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}))

    return 0
}
complete -o nospace -F _gitg gitg

_make() {
    local cur opts
    cur="${COMP_WORDS[COMP_CWORD]}"
    opts=$(cd-make-dir; make -qp 2> /dev/null | sed -n -e 's/^\([^.#[:space:]][^:[:space:]]*\): .*/\1/p' | sed '/%/ d')

    COMPREPLY=($(compgen -W "${opts}" -- ${cur}))
}
complete -F _make make

alias rcp='rsync -av --progress'
cpstat() {
    tar cf - "$1" | pv -s $(du -sb "$1" | awk '{print $1}') | (cd "$2"; tar xf -)
}
cpzstat() {
    tar czf - "$1" | pv -s $(du -sb "$1" | awk '{print $1}') | (cd "$2"; tar xzf -)
}

alias fixmouse='sudo modprobe -r psmouse && sudo modprobe psmouse'
alias fixwifi='sudo modprobe -r iwlwifi && sudo modprobe iwlwifi'
alias fixwifi2='sudo killall wpa_supplicant'
alias fixeth0='sudo modprobe -r e1000e && sudo modprobe e1000e'

function _email_job {
    echo $@ | tee /var/tmp/$$.email_job.txt
    $@ 2>&1 | tee -a /var/tmp/$$.email_job.txt

    if [ $? -eq 0 ]; then
        str="[koopa] Success: $@"
        mpack -s "$str" /var/tmp/$$.email_job.txt rwolcott88@gmail.com
    else
        str="[koopa] FAILED: $@"
        mpack -s "$str" /var/tmp/$$.email_job.txt rwolcott88@gmail.com
    fi
}
alias email-job='_email_job'

man() {
    env LESS_TERMCAP_mb=$'\E[01;31m' \
    LESS_TERMCAP_md=$'\E[01;38;5;74m' \
    LESS_TERMCAP_me=$'\E[0m' \
    LESS_TERMCAP_se=$'\E[0m' \
    LESS_TERMCAP_so=$'\E[30;43m' \
    LESS_TERMCAP_ue=$'\E[0m' \
    LESS_TERMCAP_us=$'\E[04;38;5;146m' \
    man "$@"
}

weather(){ curl -s "http://api.wunderground.com/auto/wui/geo/ForecastXML/index.xml?query=${@:-<YOURZIPORLOCATION>}"|perl -ne '/<title>([^<]+)/&&printf "%s: ",$1;/<fcttext>([^<]+)/&&print $1,"\n"';}
