# README #

This is a dotfiles repository that would ordinarily be checked out in the user's home directory. Useful scripts are
located in the "scripts" folder. The idea behind these dotfiles is that dotfiles can be backed up in a repo and the
actual dotfile (i.e. ~/.bashrc) is simply a symlink to the real file in the ~/dotfiles dir.

Be sure to run `git submodule update --init --recursive` to pull down all remotes
