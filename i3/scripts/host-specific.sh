#!/bin/bash

host=$(hostname)
echo $host

if [[ $host = "oros" ]]; then
  echo "oros"

  # synergy
  /usr/bin/synergyc -f --no-tray --debug INFO --name oros 10.120.20.163:24800

  # mouse settings
  synclient MaxTapTime=0

elif [[ $host = "citadel" ]]; then
  echo "citadel"

  # synergy
  /usr/bin/synergyc -f --no-tray --debug INFO --name citadel 10.120.20.163:24800

elif [[ $host = "citadel2" ]]; then
  echo "citadel2"

  # synergy
  /usr/bin/synergys -f --no-tray --debug INFO --name citadel2 -c /home/ryan.wolcott/.synergy2.conf --address :24800

fi
