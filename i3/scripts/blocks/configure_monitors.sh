#!/bin/sh

# first, get monitor
external_monitor=$(xrandr | grep -w 'connected' | grep -v 'eDP' | cut -d' ' -f1)

xrandr --output HDMI-1-2 --off --output HDMI-1-1 --off --output DP-1-2-1 --off --rotate normal --output eDP-1-1 --mode 1920x1080 --pos 0x1440 --rotate normal --output DP-1-6 --off --output DP-1-5 --off --output DP-1-4 --off --output DP-1-2 --off --output DP-1-1 --off

if [ -n "$external_monitor" ]; then
  max_res=$(xrandr | grep -A1 ${external_monitor} | tail -n1 | cut -d' ' -f4)
  xrandr --output ${external_monitor} --mode 2560x1440 --pos 0x0 --rotate normal --output eDP-1-1 --mode 1920x1080 --pos 0x1440 --rotate normal
fi


xsetwacom set "ELAN Touchscreen" MapToOutput HEAD-0
