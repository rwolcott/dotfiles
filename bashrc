# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# set input mode to vi
set -o vi

#export TERM=screen-256color
#if command -v tmux>/dev/null; then
#    [[ $- != *i* ]] && return
#    [[ -z "$TMUX" ]] && exec tmux
#fi

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

function parse_git_branch {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/ [\1]/"
}


hostnamecolor=$(hostname | od | tr ' ' '\n' | awk '{total = total + $1}END{print 30 + (total % 8)}')
if [ $(hostname) == "dk" ]; then
    hostnamecolor=33
fi
if [ "$color_prompt" = yes ]; then
    PS1='\[\e[1;33m\][\u@\[\e[1;${hostnamecolor}m\]\h \[\e[1;33m\]\w]\[\e[0;34m\]$(parse_git_branch)\[\e[1;33m\]\n\$>\[\e[0m\] '
else
    #PS1='\[\e[1;33m\][\u@\h \w]\[\e[0;34m\]$(parse_git_branch)\[\e[1;33m\]\$\[\e[0m\] '
    PS1='\[\e[1;33m\][\u@\[\e[1;${hostnamecolor}m\]\h \[\e[1;33m\]\w]\[\e[0;34m\]$(parse_git_branch)\[\e[1;33m\]\n\$>\[\e[0m\] '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# add sbin to path
export PATH=$PATH:/sbin

# add matlab path
export PATH=$PATH:/usr/local/MATLAB/R2011b/bin

# export common paths
export DOT_PATH=$HOME/dotfiles
export DOC_PATH=$HOME/Documents
export DESKTOP_PATH=$HOME/Desktop
export PERLS_PATH=$HOME/perls
export RAWR_PATH=$HOME/rawr
export APRIL_PATH=$HOME/april

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.git-completion.bash ]; then
    . ~/.git-completion.bash
fi


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# perls ardrone python path
#export PYTHONPATH=/usr/local/lib/python2.7/dist-packages/
#export PYTHONPATH=$PERLS_PATH/third-party/venthur_drone:$PYTHONPATH
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$APRIL_PATH/lib

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/tri/opencv/3.1.0/lib

# JAVA CLASSPATH
#alias java='java -ea -server'
#export CLASSPATH=$CLASSPATH:/usr/share/java/jogl.jar:/usr/share/java/gluegen-rt.jar:/usr/local/share/java/lcm.jar:./
#for JAR in $PERLS_PATH/build/share/java/*.jar; do
#    CLASSPATH=$CLASSPATH:$JAR
#done
#for JAR in ~/visloc/build/share/java/*.jar; do
#    CLASSPATH=$CLASSPATH:$JAR
#done
#CLASSPATH=$CLASSPATH:/usr/local/share/java/lcmspy_plugins_bot2.jar
#CLASSPATH=$CLASSPATH:$APRIL_PATH/java/april.jar


# CTAGS default settings
export CTAGS='--tag-relative=no --sort=foldcase -a -R --c++-kinds=+p --langmap=C++:+.cu --fields=+iaS --extra=+q --exclude=.git --exclude=build'

# CUDA
#export CUDA_HOME=/usr/local/cuda-7.5
#export PATH=$PATH:${CUDA_HOME}/bin
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${CUDA_HOME}/lib64
#export LIBRARY_PATH=/usr/lib/nvidia-current/:$LIBRARY_PATH
#export LD_RUN_PATH=/usr/lib/nvidia-current/:$LD_RUN_PATH
#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH+$LD_LIBRARY_PATH:}/usr/lib/nvidia-current/

#export OpenCV_DIR=/home/ryan/soft/opencv/opencv-2.4.6.1/cmake

# DRIVING
export PATH=$PATH:${HOME}/driving/bin

#export FIGNORE=${FIGNORE}:.o:.d
#export VX_FONT_PATH=$HOME/dngv/data/vx_fonts
#export VX_SHADER_PATH=$HOME/dngv/src/libraries/vx/shaders
#[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
